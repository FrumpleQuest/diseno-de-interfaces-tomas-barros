# Tarea 1

## Enunciado:

Analice la dificultad que haya tenido con un software en las últimas 2 semanas. Responda en un
párrafo cada una de las siguientes preguntas:

1. Describa brevemente el software y su función principal.
2. Describa la acción que usted como usuario intentaba realizar y la dificultad que experimentó.
3. En su opinión, ¿quéatributo de facilidad de uso es el más afectado?
4. ¿Por quéeste atributo es importante para este sistema?
5. Explique cómo ha identificado el atributo
6. ¿Midió ese atributo? Explique cómo, y si no lo hizo mida ahora explicando cómo.

## Respuestas:

1. **Spotify**: La función principal de Spotify es la de reproducción de música y podcasts. El software facilita la búsqueda y almacenamiento de música, podcasts y en ocasión, videos.
2. Tratando de acceder a la lista de álbumes que tengo descargada, pulsé en el filtro de "descargados" y no obtuve ningún resultado. Esto ocurre porque los álbumes no son considerados como "descargados" a menos que se les de un like y además se descarguen todas las canciones del album. Esto es inconsistente con el funcionamiento antiguo de la aplicación, y dificulta a mi organización de mi música.
3. El principal atributo afectado fue el aprendizaje. Como la categoria de "descargado" no funciona como es de esperar, es necesario reaprender un patrón de comportamiento que el usuario previamente conocía y era capáz de aplicar.
4. Debido a que el software tiene una gran cantidad de interfaces y funcionalidades, es necesario que cada una de estas sea intuitiva y fácil de aprender. De otra manera, el usuario termina frustrado con la interfaz y termina invirtiendo menos tiempo utilizando la aplicación.
5. Identifiqué el atributo por descarte. Sabiendo que es un problema de facilidad de uso, y acatando a la definición de facilidad de uso y su segmentación en 5 aspectos, era el único que correspondía plenamente con la problemática.
6. No medí el atributo inicialmente. Para medir el atributo le pedí a un compañero que no utiliza Spotify que ejecute la siguiente serie de pasos en mi cuenta:
   1. Búsque un álbum que le guste.
   2. Lo descargue.
   3. Elimine las canciones del album que menos le gustan

    Tras lo anterior, le pregunté si consideraría que el album esta descargado en el dispositivo. Su respuesta fue: *"No todas las canciones estan descargadas, pero sí diría que el álbum en sí si lo está."*

    Evidentemente existe un gran sesgo al solo utilizar a un usuario. Para una mejor medición sería necesario consultar con una pluralidad de usuarios, tanto inexpertos como veteranos.
